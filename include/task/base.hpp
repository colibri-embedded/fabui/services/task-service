#ifndef TASK_BASE_HPP
#define TASK_BASE_HPP

#include "types.hpp"
#include <wampcc/wampcc.h>
#include <mutex>
#include <string>

namespace fabui {

class TaskService;

enum class TaskStatus {
	IDLE,
	RUNNING,
	PAUSING,
	PAUSED,
	RESUMING,
	ABORTING,
	ABORTED,
	FINISHING,
	FINISHED
};

class TaskBase {
	public:
		TaskBase(
			TaskService* service,
			const std::string& type,
			user_id_t user_id
		);

		virtual ~TaskBase();

		virtual requirements_t requirements() =0;
		virtual wampcc::json_object getState();

		virtual void notifyStateUpdate();

		virtual bool start();
		virtual bool pause();
		virtual bool resume();
		virtual bool abort();
		virtual bool finish();

		void setTaskId(task_id_t task_id);
		task_id_t getTaskId();

		void setUserId(user_id_t user_id);
		user_id_t getUserId();

		bool isRunning();

	protected:
		TaskService* m_service;

		std::string m_type;
		task_id_t 	m_task_id;
		user_id_t 	m_user_id;

		TaskStatus  m_status;
		std::mutex  m_mutex;

		float m_progress;

		void changeStatus(TaskStatus status, bool postpone_update=false);
		TaskStatus getStatus();

		void changeProgress(float progress, bool postpone_update=false);
		float getProgress();
};

} // namespace fabui

#endif /* TASK_BASE_HPP */
