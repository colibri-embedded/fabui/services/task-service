#ifndef TASK_FABRICATION_HPP
#define TASK_FABRICATION_HPP

#include "task/base.hpp"
#include <wampcc/wampcc.h>

#define FABRICATION_TASK "fabrication"

namespace fabui {

class FabricationTask : public TaskBase {
	public:
		FabricationTask(
			TaskService* service,
			const std::string& type,
			user_id_t user_id,
			const FileInfo& file
		);

		virtual ~FabricationTask();

		virtual requirements_t requirements() =0;
		virtual wampcc::json_object getState();

		bool start();
		bool finish();
		bool abort();
		bool pause();
		bool resume();

	protected:
		FileInfo m_file_info;
		float m_file_progress;
		wampcc::t_subscription_id m_subscription_id;

		virtual void gcode_file_event(wampcc::event_info);
};

} // namespace fabui

#endif /* TASK_FABRICATION_HPP */