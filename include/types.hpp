#ifndef TASK_SERVICE_TYPES_HPP
#define TASK_SERVICE_TYPES_HPP

#include <string>
#include <vector>

namespace fabui {

	typedef unsigned task_id_t;
	typedef unsigned user_id_t;
	typedef std::vector<std::string> requirements_t;

	struct FileInfo {
		unsigned id;
		std::string filename;
		std::string name;
		std::string ext;
	};

} // namespace fabui

#endif /* TASK_SERVICE_TYPES_HPP */