#ifndef TASK_SERVICE_HPP
#define TASK_SERVICE_HPP

#include <algorithm>
#include <functional>
#include <string>
#include <thread>
#include <memory>
#include <vector>
#include <mutex>
#include <chrono>

#include <fabui/http/client.hpp>
#include <fabui/wamp/session.hpp>
#include <fabui/thread/event.hpp>

#include "types.hpp"
#include "task/fabrication.hpp"

namespace fabui {

class TaskService {
	public:
		/**
		 * Create TaskService.
		 *
		 * @param storage_path
		 */
		TaskService(const std::string& storage_path);

		/// Destroy TaskService
		~TaskService();

		//void terminate();

		/* Task functions */

		/**
		 *
		 */
		//bool create(user_id_t user_id, const std::string& type, const std::string& controller);

		/**
		 *
		 */
		//bool updateStatus(task_id_t task_id, TaskStatus status);

		/**
		 *
		 */
		bool start(user_id_t user_id, const std::string& type, const std::string& controller, const std::string& file);

		/**
		 *
		 */
		bool abort();

		/**
		 *
		 */
		bool pause();

		/**
		 *
		 */
		bool resume();

		/**
		 *
		 */
		wampcc::json_object info();

		/**
		 *
		 */
		void setWampSession(IWampSession* session);

		/**
		 *
		 */
		IWampSession* getWampSession();

		/**
		 *
		 */
		void setHttpClient(IHttpClient *http_client);

		/**
		 *
		 */
		IHttpClient* getHttpClient();

	private:
		IWampSession *m_session;
		IHttpClient *m_http;

		/* current task */
		std::mutex m_task_mut;
		std::unique_ptr<FabricationTask> currentTask;

		/* threads */
		Event e_start;
		Event e_finish;

		std::mutex m_running_mut;
		bool m_running;

		std::string m_storage_path;
};

} // namespace fabui

#endif /* TASK_SERVICE_HPP */
