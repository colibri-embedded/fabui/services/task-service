#ifndef TOPICS_HPP
#define TOPICS_HPP

#define TOPIC_GCODE 		"gcode.events"
#define TOPIC_GCODE_FILE	"gcode.file.events"

#define TOPIC_TASK          "task.events"

#define TASK_STATE_UPDATE   "state.update"

#endif /* TOPICS_HPP */
