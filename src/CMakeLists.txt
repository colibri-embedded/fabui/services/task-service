set(SOURCES
	"${PROJECT_SOURCE_DIR}/src/main.cpp"
	"${PROJECT_SOURCE_DIR}/src/task_service.cpp"
	"${PROJECT_SOURCE_DIR}/src/task_service_wamp_wrapper.cpp"

	"${PROJECT_SOURCE_DIR}/src/task/base.cpp"
	"${PROJECT_SOURCE_DIR}/src/task/fabrication.cpp"
	"${PROJECT_SOURCE_DIR}/src/task/print.cpp"
	)

add_executable(task-service ${SOURCES})
set_property(TARGET task-service PROPERTY CXX_STANDARD 14)
set_property(TARGET task-service PROPERTY CXX_STANDARD_REQUIRED ON)

target_include_directories(task-service PUBLIC ${3RDPARTY_SOURCE_DIR})
target_include_directories(task-service PUBLIC ${CURL_INCLUDE_DIR})

add_definitions(${WAMPCC_CFLAGS} ${WAMPCC_CFLAGS_OTHER})

target_link_libraries (task-service ${LIBFABUI_LIBRARIES})

##
## Install targets
##
install (TARGETS task-service DESTINATION "${INSTALL_BIN_DIR}")
