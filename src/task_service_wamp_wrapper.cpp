/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: task_service_wamp_wrapper.cpp
 * @brief TaskService wamp wrapper implementation
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#include "config.h"
#include "task_service_wamp_wrapper.hpp"

using namespace fabui;
using namespace wampcc;
using namespace std::placeholders;

TaskServiceWrapper::TaskServiceWrapper(const std::string& env_file, TaskService& service)
	: BackendService(PACKAGE_STRING, env_file, 2, false)
	, m_service(service)
{
	m_service.setWampSession(this);
	m_service.setHttpClient(&http);
}

void TaskServiceWrapper::onConnect()
{
	join( realm(), {"token"}, "task-service");
}

void TaskServiceWrapper::onDisconnect()
{
	// m_service.stop();
}

void TaskServiceWrapper::onJoin()
{
	std::cout << "Joined\n";
	auto info = call("backend.get_url");
	info.wait();
	auto result = info.get();
	if(!result.was_error) {
		http.setBaseUrl( result.args.args_list[0].as_string() );
	}

	// m_running = true;
	//m_runner_thread = std::thread(&TaskService::runnerThreadLoop, this);

	provide("task.service.terminate", std::bind(&TaskServiceWrapper::terminate_wrapper, this, _1));

	provide("task.start", 	std::bind(&TaskServiceWrapper::start_wrapper, this, _1));
	provide("task.abort", 	std::bind(&TaskServiceWrapper::abort_wrapper, this, _1));
	provide("task.pause", 	std::bind(&TaskServiceWrapper::pause_wrapper, this, _1));
	provide("task.resume", 	std::bind(&TaskServiceWrapper::resume_wrapper, this, _1));
	provide("task.info",	std::bind(&TaskServiceWrapper::info_wrapper, this, _1));

	publish("task.events", { {"task_service_started"} }, {});

	std::cout << PACKAGE_NAME" started\n" << std::flush;
}

void TaskServiceWrapper::terminate_wrapper(invocation_info info) {
	yield(info.request_id, {true});
	disconnect();
}

void TaskServiceWrapper::start_wrapper(invocation_info info) {
	try {
		/* Get Caller Identity */
		user_id_t user_id 			= std::stoi( getCallerIdOrFail(info.details) );
		/* Get arguments */
		std::string task_type 		= getArgumentOrFail("type", info.args.args_dict).as_string();
		std::string task_controller = getArgumentOrFail("controller", info.args.args_dict).as_string();
		std::string task_file_uri	= getArgumentOrFail("file_uri", info.args.args_dict).as_string();
		/* Run task.start(...) */
		auto result = m_service.start(user_id, task_type, task_controller, task_file_uri);
		/* Return result */
		yield(info.request_id, {result});
	} catch(const std::exception &e) {
		/* Return error */
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}
}

void TaskServiceWrapper::abort_wrapper(invocation_info info) {
	std::cout << "Abort request\n";
	try {
		auto result = m_service.abort();
		yield(info.request_id, {result});
	} catch(const std::exception &e) {
		/* Return error */
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}
}

void TaskServiceWrapper::pause_wrapper(invocation_info info) {
	try {
		auto result = m_service.pause();
		yield(info.request_id, {result});
	} catch(const std::exception &e) {
		/* Return error */
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}
}

void TaskServiceWrapper::resume_wrapper(invocation_info info) {
	try {
		auto result = m_service.resume();
		yield(info.request_id, {result});
	} catch(const std::exception &e) {
		/* Return error */
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}
}

void TaskServiceWrapper::info_wrapper(invocation_info info) {
	try {
		yield(info.request_id, {}, m_service.info());
	} catch(const std::exception &e) {
		/* Return error */
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}
}
