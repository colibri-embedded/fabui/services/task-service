#include "task/base.hpp"
#include "task_service.hpp"
#include "types.hpp"
#include "backend_api.hpp"
#include <fabui/wamp/topics.hpp>
#include <fabui/wamp/procedures.hpp>

using namespace fabui;
using namespace wampcc;
using namespace std::placeholders;

FabricationTask::FabricationTask(TaskService* service, const std::string& type, user_id_t user_id, const FileInfo& file)
	: TaskBase(service, type, user_id)
	, m_file_info(file)
{
	//throw std::runtime_error("just because");

	// std::cout << "FabricationTask.TaskId: " << getTaskId() << std::endl << std::flush;

	// add file resource to task
}

FabricationTask::~FabricationTask()
{

}

json_object FabricationTask::getState() {
	auto state = TaskBase::getState();

	json_object file = {
		{ "name", m_file_info.name },
		{ "progress", m_file_progress}
	};

	state["file"] = std::move(file);

	return state;
}

void FabricationTask::gcode_file_event(wampcc::event_info info) {
	// std::cout << "FabricationTask::gcode.file.event: " << info.args.args_list << ", " << info.args.args_dict << std::endl << std::flush;

	std::string action = info.args.args_list[0].as_string();
	if(action == "progress_update") {
		m_file_progress = info.args.args_dict["progress"].as_real();
		changeProgress(m_file_progress);
		//auto state = getState();
		//m_service->publish("task.events", { {"state_update"}, state});
	}

}

bool FabricationTask::start() {
	auto status = getStatus();
	if(status != TaskStatus::IDLE)
		return false;

	bool result = false;

	m_file_progress = 0.0f;

	auto sfut = m_service->getWampSession()->subscribe(TOPIC_GCODE_FILE, std::bind(&FabricationTask::gcode_file_event, this, _1), {});
	sfut.wait();
	m_subscription_id = sfut.get().subscription_id;

	auto cfut = m_service->getWampSession()->call(PROCEDURE_FILE_LOAD, { { m_file_info.filename } }, {});
	cfut.wait();

	/*cfut = m_service->getWampSession()->call("gcode.file.info");
	cfut.wait();*/

	result = TaskBase::start();

	cfut = m_service->getWampSession()->call(PROCEDURE_FILE_PUSH, {}, {});
	cfut.wait();

	return result;
}

bool FabricationTask::finish() {
	auto result = TaskBase::finish();
	return result;
}

bool FabricationTask::abort() {
	// std::cout << "FabricationTask::abort\n";
	auto status = getStatus();
	if(	status == TaskStatus::ABORTING  ||
		status == TaskStatus::ABORTED ||
		status == TaskStatus::FINISHING ||
		status == TaskStatus::FINISHED ||
		status == TaskStatus::IDLE )
	{
		// std::cout << "FabricationTask::abort rejected: " << int(status) << std::endl;
		return false;
	}

	//std::cout << "FabricationTask::status " << int(status) << std::endl;

	// std::cout << "TASK aborting...\n";

	changeStatus(TaskStatus::ABORTING);

	auto cfut = m_service->getWampSession()->call(PROCEDURE_FILE_ABORT, {}, {});
	cfut.wait();
	auto result = cfut.get();

	if(result.was_error) {
		std::cout << "Task aborting FAILED\n";
		return false;
	} else {
		if(result.args.args_list.size() != 1)
			return false;
		bool normal_reset = result.args.args_list[0].as_bool();
		if(!normal_reset) {
			changeStatus(TaskStatus::ABORTED);
			// std::cout << "TASK aborted\n";
			return true;
		}
	}

	cfut = m_service->getWampSession()->call(PROCEDURE_MACRO_RUN, { { m_type + ".abort" } }, {});
	cfut.wait();

	changeStatus(TaskStatus::ABORTED);
	// std::cout << "TASK aborted\n";

	return true;
}

bool FabricationTask::pause() {
	return TaskBase::pause();
}

bool FabricationTask::resume() {
	return TaskBase::resume();
}
