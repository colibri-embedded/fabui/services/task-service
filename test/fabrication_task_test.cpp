#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include <task_service.hpp>

#include <future>
#include <vector>
#include <memory>
#include <map>
#include <fabui/wamp/mock/session.hpp>
#include <fabui/http/mock/client.hpp>
#include <fabui/wamp/topics.hpp>
#include <fabui/wamp/procedures.hpp>

#include <task/base.hpp>
#include "backend_api.hpp"
#include "topics.hpp"

using namespace fabui;
using namespace std::placeholders;
using namespace std::literals;

class FabricationTaskSpy: public FabricationTask {
	public:
		FabricationTaskSpy(TaskService* service, const std::string& type, user_id_t user_id, const FileInfo& file)
			: FabricationTask(service, type, user_id, file)
		{

		}

		requirements_t requirements() {
			return {"head:print", "extruder"};
		}

		TaskStatus getStatus() {
			return TaskBase::getStatus();
		}

		float getProgress() {
			return TaskBase::getProgress();
		}

		void changeStatus(TaskStatus status, bool postpone_update=false) {
			return TaskBase::changeStatus(status, postpone_update);
		}

		void changeProgress(float progress, bool postpone_update=false) {
			return TaskBase::changeProgress(progress, postpone_update);
		}

		void overrideStatus(TaskStatus status) {
			m_status = status;
		}

		void overrideProgress(float progress) {
			m_progress = progress;
		}
};

class FabuiBackendMock: public HttpClientMock {
	public:
		FabuiBackendMock()
		: HttpClientMock() {
			addResponder(API_TASK_CREATE, [](
					HttpMethod http_method,
					const std::string& sub_url,
					const std::string& data,
					const IHttpClient::params_t& params,
					const IHttpClient::headers_t& headers) -> HttpReponse {

				return HttpReponse{HTTP_CREATED, "{\"id\":1}", ""};
			});
		}
};

/***** TESTS *****/
SCENARIO("New task creation", "[fabrication-task][creation]") {

	WHEN("Create a new task with failed http put") {
		TaskService ts((STORAGE_DIR));
		HttpClientMock http;
		ts.setHttpClient(&http);
        FileInfo file_info{1, "8999bbfda4f0a6206ddea275339a9ec2.gcode", "printable.gcode", "gcode"};
		FabricationTaskSpy *fbs;

		THEN("Object is not created") {
			REQUIRE_THROWS_WITH( fbs = new FabricationTaskSpy(&ts, "fabrication", 1, file_info), "failed to create a new task resource");
		}
	}

	WHEN("Create a new task with successfull http put") {
		TaskService ts(STORAGE_DIR);
		FabuiBackendMock http;
		ts.setHttpClient(&http);
        FileInfo file_info{1, "8999bbfda4f0a6206ddea275339a9ec2.gcode", "printable.gcode", "gcode"};

		THEN("Object is created") {
			FabricationTaskSpy *fbs;
			REQUIRE_NOTHROW( fbs = new FabricationTaskSpy(&ts, "fabrication", 1, file_info), "failed to create a new task resource");
			REQUIRE_NOTHROW( delete fbs );
		}
	}

}

SCENARIO("Task starting", "[fabrication-task][start]") {
    TaskService ts(STORAGE_DIR);
    FabuiBackendMock http;
    ts.setHttpClient(&http);
    WampSessionMock wamp;
	ts.setWampSession(&wamp);
    FileInfo file_info{1, "8999bbfda4f0a6206ddea275339a9ec2.gcode", "printable.gcode", "gcode"};

    WHEN("start is called on a new task") {
        FabricationTaskSpy fbs(&ts, "fabrication", 1, file_info);
        auto fut1 = wamp.getCallMatchFuture(PROCEDURE_FILE_LOAD);
        auto fut2 = wamp.getCallMatchFuture(PROCEDURE_MACRO_RUN);
		auto fut3 = wamp.getCallMatchFuture(PROCEDURE_FILE_PUSH);

        THEN("task is started") {
            REQUIRE( fbs.start() );
            REQUIRE( fbs.isRunning() );

            auto status = fut1.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );

            status = fut2.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );

            status = fut3.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );

            REQUIRE( wamp.m_subscribed.size() == 1);
            REQUIRE( wamp.m_subscribed[0].topic == TOPIC_GCODE_FILE);
        }
    }

    WHEN("start is called on a started task") {
        FabricationTaskSpy fbs(&ts, "fabrication", 1, file_info);

        THEN("start returns false") {
            fbs.overrideStatus(TaskStatus::RUNNING);
            REQUIRE_FALSE( fbs.start() );
        }
    }

}

SCENARIO("File progress update is published","[fabrication-task][file-update]") {
    TaskService ts(STORAGE_DIR);
    FabuiBackendMock http;
    ts.setHttpClient(&http);
    WampSessionMock wamp;
	ts.setWampSession(&wamp);
    FileInfo file_info{1, "8999bbfda4f0a6206ddea275339a9ec2.gcode", "printable.gcode", "gcode"};

    WHEN("File progress update is pushed") {
        auto fut1 = wamp.getPublishMatchFuture(TOPIC_TASK, 2);
        FabricationTaskSpy fbs(&ts, "fabrication", 1, file_info);

        THEN("Progress update is received and pushed on task.events") {
            REQUIRE( fbs.start() );

            wamp.publish_to_topic(TOPIC_GCODE_FILE, { {"progress_update"}, {
                {"progress", 20.0f}
            }} );

            auto status = fut1.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );

            REQUIRE( wamp.m_published[1].args.args_list[0].as_string() == "state.update" );
            auto state = wamp.m_published[1].args.args_dict;

            REQUIRE( state["progress"].as_real() == 20.0f );
        }
    }

}

SCENARIO("Fabrication task state","[fabrication-task][read-state]") {
    TaskService ts(STORAGE_DIR);
    FabuiBackendMock http;
    ts.setHttpClient(&http);
    WampSessionMock wamp;
	ts.setWampSession(&wamp);
    FileInfo file_info{1, "8999bbfda4f0a6206ddea275339a9ec2.gcode", "printable.gcode", "gcode"};
    FabricationTaskSpy fbs(&ts, "fabrication", 1, file_info);

    WHEN("getState is called") {
        FabricationTaskSpy fbs(&ts, "fabrication", 1, file_info);

        THEN("state with file info is returned") {
            REQUIRE( fbs.start() );
            auto state = fbs.getState();

            REQUIRE( state["file"].is_object() );
            REQUIRE( state["file"]["name"].as_string() == "printable.gcode" );
            REQUIRE( state["file"]["progress"].as_real() == 0.0f );
        }
    }

}

SCENARIO("Fabrication task abort","[fabrication-task][abort]") {

    TaskService ts(STORAGE_DIR);
    FabuiBackendMock http;
    ts.setHttpClient(&http);
    WampSessionMock wamp;
	ts.setWampSession(&wamp);
    FileInfo file_info{1, "8999bbfda4f0a6206ddea275339a9ec2.gcode", "printable.gcode", "gcode"};
    FabricationTaskSpy fbs(&ts, "fabrication", 1, file_info);

    wamp.addCallResponder(PROCEDURE_FILE_ABORT, [](wampcc::wamp_args args) -> wampcc::result_info {
        return wampcc::result_info(
                0,      // request_id
                {},     // additional
                wampcc::wamp_args{
                    {true},
                    {}
                },
                nullptr
        );
    });

    WHEN("abort is called on an idle task") {

	    THEN("abort returns false") {
			fbs.overrideStatus(TaskStatus::IDLE);
			REQUIRE_FALSE( fbs.abort() );
	    }
	}

	WHEN("abort is called on a started task") {

		auto fut1 = wamp.getPublishMatchFuture(TOPIC_TASK, 2);
		auto fut2 = wamp.getCallMatchFuture(PROCEDURE_FILE_ABORT);
        auto fut3 = wamp.getCallMatchFuture(PROCEDURE_MACRO_RUN);

	    THEN("task is aborted") {
            fbs.overrideStatus(TaskStatus::RUNNING);
			REQUIRE( fbs.abort() );

			auto status = fut1.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );
            REQUIRE( wamp.m_published[0].args.args_list[0].as_string() == TASK_STATE_UPDATE);
			auto state = wamp.m_published[0].args.args_dict;
			REQUIRE( state["status"] == "ABORTING" );
            REQUIRE( wamp.m_published[1].args.args_list[0].as_string() == TASK_STATE_UPDATE);
			state = wamp.m_published[1].args.args_dict;
			REQUIRE( state["status"] == "ABORTED" );

            status = fut2.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );

            status = fut3.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );
			REQUIRE( wamp.m_called[1].args.args_list[0].as_string() == "fabrication.abort");
	    }
	}

	WHEN("abort is called on a paused task") {
    	auto fut1 = wamp.getPublishMatchFuture(TOPIC_TASK, 2);
		auto fut2 = wamp.getCallMatchFuture(PROCEDURE_FILE_ABORT);
        auto fut3 = wamp.getCallMatchFuture(PROCEDURE_MACRO_RUN);

	    THEN("task is aborted") {
			fbs.overrideStatus(TaskStatus::PAUSED);
			REQUIRE( fbs.abort() );

    		auto status = fut1.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );
            REQUIRE( wamp.m_published[0].args.args_list[0].as_string() == TASK_STATE_UPDATE);
			auto state = wamp.m_published[0].args.args_dict;
			REQUIRE( state["status"] == "ABORTING" );
            REQUIRE( wamp.m_published[1].args.args_list[0].as_string() == TASK_STATE_UPDATE);
			state = wamp.m_published[1].args.args_dict;
			REQUIRE( state["status"] == "ABORTED" );

            status = fut2.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );

            status = fut3.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );
			REQUIRE( wamp.m_called[1].args.args_list[0].as_string() == "fabrication.abort");
	    }
	}

	WHEN("abort is called on a aborted task") {

	    THEN("abort returns false") {
			fbs.overrideStatus(TaskStatus::ABORTED);
			REQUIRE_FALSE( fbs.abort() );
	    }
	}

	WHEN("abort is called on a finished task") {

	    THEN("abort returns false") {
			fbs.overrideStatus(TaskStatus::FINISHED);
			REQUIRE_FALSE( fbs.abort() );
	    }
	}

}
