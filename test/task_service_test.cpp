#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include <task_service.hpp>

#include <future>
#include <vector>
#include <memory>
#include <map>
#include <fabui/wamp/mock/session.hpp>
#include <fabui/http/mock/client.hpp>

using namespace fabui;
using namespace std::placeholders;
using namespace std::literals;

/****** Constants *******/

#define TIC std::chrono::high_resolution_clock::duration tic_time = std::chrono::high_resolution_clock::now().time_since_epoch()
#define TOC std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()-tic_time).count()

class Delayed {
	public:
		Delayed(unsigned delay, std::function<void()> callback)
			: m_delay(delay)
			, m_callback(callback)
		{

		}

		~Delayed() {
			if(m_thread.joinable())
				m_thread.join();
		}

		void start() {
			m_thread = std::thread(&Delayed::loop, this);
		}

		void loop() {
			std::this_thread::sleep_for( std::chrono::milliseconds(m_delay)  );
			m_callback();
		}

		std::thread m_thread;

		unsigned m_delay;
		std::function<void()> m_callback;
};

/***** TESTS *****/

SCENARIO("New object creation", "[machine-service][creation]") {

	WHEN("New object is created -> destroyed") {
		TaskService *ts = new TaskService(STORAGE_DIR);

		THEN("Object is destroyed successfully") {
			delete ts;
		}
	}

}

SCENARIO("Start a new task", "[machine-service][start]") {

	WHEN("Start is called") {
		TaskService *ts = new TaskService(STORAGE_DIR);
		HttpClientMock http;
		WampSessionMock wamp;

		http.addResponder("/v1/storages/local/files/1", [](HttpMethod http_method,
				const std::string& sub_url,
				const std::string& data,
				const IHttpClient::params_t& params,
				const IHttpClient::headers_t& headers) -> HttpReponse {

			std::cout << "GET" << sub_url << std::endl;

			return HttpReponse{200, "{\"id\":1, \"filename\":\"8999bbfda4f0a6206ddea275339a9ec2.gcode\", \"ext\":\".gcode\",\"name\":\"printable.gcode\"}", ""};

		});

		ts->setWampSession(&wamp);
		ts->setHttpClient(&http);

		THEN("Task is started") {
			//bool start(user_id_t user_id, const std::string& type, const std::string& controller, const std::string& file);
			REQUIRE( ts->start(1, "print", "make-print", "file://local/1") );


			delete ts;
		}
	}

}
